import * as dayjs from 'dayjs';

export function generateAge(birthday: Date): number {
  const today = dayjs(new Date());
  return today.diff(dayjs(dayjs(birthday).format('YYYY-MM-DD')), 'year');
}
