import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { Request } from 'express';
import { read, utils } from 'xlsx';

export const fileFilter = (
  req: Request,
  file: Express.Multer.File,
  callback: (error: Error, acceptFile: boolean) => void,
) => {
  if (!file.originalname.match(/\.(xlsx)$/)) {
    req.body.errorMessages = 'Verifica que tu archivo tenga formato .XLSX';
    callback(null, false);
  }
  callback(null, true);
};

export const fileOptions: MulterOptions = {
  limits: { fileSize: Infinity },
  fileFilter: fileFilter,
};

export function getDataExceltoJson(
  file: Express.Multer.File,
  skipt: number,
  sheetName: string,
) {
  const data = read(file.buffer, { type: 'buffer' });
  let dataSheet = utils.sheet_to_json(data.Sheets[sheetName], {
    header: 'A',
    blankrows: false,
    defval: null,
  });

  dataSheet = dataSheet.slice(skipt);

  return dataSheet;
}
