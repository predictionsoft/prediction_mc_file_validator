import { Injectable } from '@nestjs/common';
import { CreateRecordDto } from './dto/create-record.dto';
import { InjectModel } from '@nestjs/mongoose';
import { RecordDocument, Records } from './schema/record.schema';
import { Model } from 'mongoose';

@Injectable()
export class RecordService {
  constructor(
    @InjectModel(Records.name) private recordsModel: Model<RecordDocument>,
  ) {}

  async create(createRecordDto: CreateRecordDto) {
    return await this.recordsModel.create(createRecordDto);
  }

  async findAll() {
    return await this.recordsModel.find();
  }

  async remove() {
    return await this.recordsModel.deleteMany();
  }
}
