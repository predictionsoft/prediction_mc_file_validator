import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import { Transform } from 'class-transformer';

export type RecordDocument = Records & Document;

@Schema({ timestamps: true })
export class Records {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop()
  month: string;

  @Prop()
  year: string;

  @Prop()
  createdBy: string;
}
export const RecordSchema = SchemaFactory.createForClass(Records);
