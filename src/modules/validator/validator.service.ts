import { Injectable } from '@nestjs/common';
import { Context } from 'src/modules/validator/pattern/strategies/context.strategy';
import { SalesStrategy } from 'src/modules/validator/pattern/strategies/sales.strategy';
import { StrategyResponse } from 'src/modules/validator/pattern/strategy.interface';

@Injectable()
export class ValidatorService {
  getStrategy = {
    sales: () => new SalesStrategy(),
  };

  constructor(private context: Context) {}

  async validateFile(
    file: Express.Multer.File,
    strategy: string,
  ): Promise<StrategyResponse> {
    this.context.setStrategy(this.getStrategy[strategy]());
    return this.context.validateFile(file);
  }
}
