import {
  Controller,
  Post,
  Req,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ValidatorService } from './validator.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { fileOptions } from 'src/utils/file.helper';
import { StrategyResponse } from 'src/modules/validator/pattern/strategy.interface';
import { Request } from 'express';

@Controller('validator')
export class ValidatorController {
  constructor(private readonly validatorService: ValidatorService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file', fileOptions))
  async validate(
    @Req() req: Request,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<StrategyResponse> {
    const strategy = req.body.strategy;
    if (req.body.errorMessages) {
      return {
        success: false,
        listObjects: [],
        errorMapping: [],
        errorMessage: {
          title: 'Formato incorrecto',
          message: req.body.errorMessages,
        },
      };
    }
    return this.validatorService.validateFile(file, strategy);
  }

  @Post('save')
  @UseInterceptors(FileInterceptor('file', fileOptions))
  async validSave(
    @Req() req: Request,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<StrategyResponse> {
    const strategy = req.body.strategy;
    if (req.body.errorMessages) {
      return {
        success: false,
        listObjects: [],
        errorMapping: [],
        errorMessage: {
          title: 'Formato incorrecto',
          message: req.body.errorMessages,
        },
      };
    }
    return this.validatorService.validateFile(file, strategy);
  }
}
