import { Module } from '@nestjs/common';
import { ValidatorService } from './validator.service';
import { ValidatorController } from './validator.controller';
import { Context } from 'src/modules/validator/pattern/strategies/context.strategy';

@Module({
  controllers: [ValidatorController],
  providers: [ValidatorService, Context],
})
export class ValidatorModule {}
