import { SalesValidatorDto } from 'src/shared/dto/sales-validator.dto';

export interface Strategy {
  validateFile(file: Express.Multer.File): Promise<void>;
  getFieldsErrors(): ErrorMapping[];
  getFileErrors();
  getValidatedData();
}

export interface StrategyResponse {
  listObjects: Array<SalesValidatorDto>;
  errorMapping?: ErrorMapping[];
  errorMessage?: ErrorFiles;
  success: boolean;
}

export interface ErrorMapping {
  reason: string[];
  item: SalesValidatorDto;
}

export interface ErrorFiles {
  title: string;
  message: string;
}
