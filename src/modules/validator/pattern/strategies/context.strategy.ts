import { Strategy, StrategyResponse } from '../strategy.interface';

export class Context {
  private strategy: Strategy;

  constructor(strategy: Strategy) {
    this.strategy = strategy;
  }

  public setStrategy(strategy: Strategy) {
    this.strategy = strategy;
  }

  public async validateFile(
    file: Express.Multer.File,
  ): Promise<StrategyResponse> {
    await this.strategy.validateFile(file);
    return {
      success:
        this.strategy.getFieldsErrors().length === 0 &&
        this.strategy.getFileErrors() === null,
      listObjects: this.strategy.getValidatedData(),
      errorMapping: this.strategy.getFieldsErrors(),
      errorMessage: this.strategy.getFileErrors(),
    };
  }
}
