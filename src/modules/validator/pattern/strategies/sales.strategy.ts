import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { getDataExceltoJson } from '../../../../utils/file.helper';
import { ErrorFiles, ErrorMapping } from '../strategy.interface';
import { Strategy } from '../strategy.interface';
import { SalesValidatorDto } from 'src/shared/dto/sales-validator.dto';
import { generateAge } from 'src/utils/common';

const MAXIMUN_LENGTH_FILE = 505;
const MINIMUN_LENGTH_FILE = 0;

@Injectable()
export class SalesStrategy implements Strategy {
  private errorFieldsMapping: Array<ErrorMapping> = [];
  private validatesObjects: Array<SalesValidatorDto> = [];
  private errorMessages: ErrorFiles;

  getFieldsErrors(): Array<ErrorMapping> {
    return this.errorFieldsMapping;
  }

  getValidatedData() {
    return this.validatesObjects;
  }

  getFileErrors() {
    return this.errorMessages;
  }

  async validateFile(file: Express.Multer.File): Promise<void> {
    const data = getDataExceltoJson(file, 1, 'ventas');
    this.isValidLength(data)
      ? await this.mapResponseDataFieldsFile(data)
      : this.mapResponseDataLenghtFile(data);
  }

  private async mapResponseDataFieldsFile(data: Array<unknown>) {
    const dataTransform = this.getFomatedData(data);
    const validations: SalesValidatorDto[] = plainToInstance(
      SalesValidatorDto,
      dataTransform,
      {
        excludeExtraneousValues: false,
      },
    );
    for (const valid of validations) {
      const errors = await validate(valid);
      const errorFlatten = errors.flatMap((el) =>
        Object.values(el.constraints),
      );
      if (errorFlatten.length > 0)
        this.errorFieldsMapping.push({ reason: errorFlatten, item: valid });
    }
    if (this.errorFieldsMapping.length === 0)
      this.validatesObjects = validations;

    this.errorMessages =
      this.errorFieldsMapping.length > 0 || dataTransform.length === 0
        ? {
            title: 'El archivo presenta errores',
            message: 'Corrige el formato',
          }
        : null;
  }

  private getFomatedData(
    dataPayouts: Array<unknown>,
  ): Array<SalesValidatorDto> {
    return dataPayouts.reduce((acc: Array<SalesValidatorDto>, item) => {
      const values = Object.values(item);
      values.shift();
      if (values.length > 0 && values.some((val) => val != null)) {
        acc.push({
          dateBilling: item['A'],
          birthdate: item['B'],
          age: generateAge(new Date(item['B'])),
          gender: item['C'],
          amount: +item['D'],
        });
      }
      return acc;
    }, []) as Array<SalesValidatorDto>;
  }

  private isValidLength(data: Array<unknown>): boolean {
    return (
      data.length < MAXIMUN_LENGTH_FILE && data.length > MINIMUN_LENGTH_FILE
    );
  }

  private mapResponseDataLenghtFile(data: Array<unknown>) {
    this.errorMessages = {
      title:
        data.length > MAXIMUN_LENGTH_FILE
          ? 'Se superó el número de registros'
          : 'El archivo no tiene datos',
      message:
        data.length > MAXIMUN_LENGTH_FILE
          ? `Verifica que tu archivo tenga menos de ${MAXIMUN_LENGTH_FILE} registros.`
          : 'Ingresa la información necesaria para crear registros.',
    };
  }
}
