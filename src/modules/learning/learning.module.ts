import { Module } from '@nestjs/common';
import { LearningService } from './learning.service';
import { LearningController } from './learning.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Sales, SalesSchema } from './schema/sales.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Sales.name, schema: SalesSchema }]),
  ],
  controllers: [LearningController],
  providers: [LearningService],
})
export class LearningModule {}
