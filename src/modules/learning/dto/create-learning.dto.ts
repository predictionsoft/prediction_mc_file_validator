import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Matches,
  ValidateNested,
} from 'class-validator';
import { Gender } from 'src/shared/dto/sales-validator.dto';

export class SalesDto {
  @IsNotEmpty()
  @Matches(/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/, {
    message: 'dateBilling format date yyyy-mm-dd',
  })
  dateBilling: string;

  @IsEnum(Gender, {
    message: `gender must be one of the following: ${Object.values(Gender).join(
      ', ',
    )}.`,
  })
  @IsNotEmpty()
  gender: Gender;

  @IsOptional()
  @Matches(/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/, {
    message: 'birthdate format date yyyy-mm-dd',
  })
  birthdate: string;

  @IsOptional()
  @IsNumber()
  age: number;

  @IsNotEmpty()
  @IsNumber()
  amount: number;
}

export class CreateSalesDto {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => SalesDto)
  sales: SalesDto[];
}
