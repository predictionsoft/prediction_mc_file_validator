import { Injectable } from '@nestjs/common';
import { SalesDto } from './dto/create-learning.dto';
import { Sales, SalesDocument } from './schema/sales.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class LearningService {
  constructor(
    @InjectModel(Sales.name) private salesModel: Model<SalesDocument>,
  ) {}

  async create(
    salesDto: Array<SalesDto> | SalesDto,
  ): Promise<Array<SalesDocument> | SalesDocument> {
    return await this.salesModel.create(salesDto);
  }

  async findAll(): Promise<Array<SalesDocument>> {
    return await this.salesModel.find();
  }

  async delete() {
    return await this.salesModel.deleteMany();
  }
}
