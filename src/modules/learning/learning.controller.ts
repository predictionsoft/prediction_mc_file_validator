import { Controller, Get, Post, Body, Delete } from '@nestjs/common';
import { LearningService } from './learning.service';
import { CreateSalesDto } from './dto/create-learning.dto';

@Controller('learning-save')
export class LearningController {
  constructor(private readonly learningService: LearningService) {}

  @Post()
  create(
    @Body()
    salesDtos: CreateSalesDto,
  ) {
    return this.learningService.create(salesDtos.sales);
  }

  @Get()
  findAll() {
    return this.learningService.findAll();
  }

  @Delete()
  delete() {
    return this.learningService.delete();
  }
}
