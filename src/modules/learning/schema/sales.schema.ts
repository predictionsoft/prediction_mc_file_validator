import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import { Transform } from 'class-transformer';
import { Gender } from 'src/shared/dto/sales-validator.dto';

export type SalesDocument = Sales & Document;

@Schema({ timestamps: true })
export class Sales {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop()
  dateBilling: Date;

  @Prop({ type: String, enum: Gender })
  gender: Gender;

  @Prop()
  birthdate?: Date;

  @Prop()
  age?: number;

  @Prop()
  amount: number;
}
export const SalesSchema = SchemaFactory.createForClass(Sales);
