import { Module } from '@nestjs/common';
import { ValidatorModule } from './modules/validator/validator.module';
import { ConfigModule } from '@nestjs/config';
import { LearningModule } from './modules/learning/learning.module';
import { MongooseModule } from '@nestjs/mongoose';
import { RecordModule } from './modules/record/record.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: () => ({
        uri: process.env.DB_URI,
      }),
    }),
    ValidatorModule,
    LearningModule,
    RecordModule,
  ],
  controllers: [],
})
export class AppModule {}
