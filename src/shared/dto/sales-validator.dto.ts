import { IsEnum, IsNotEmpty, IsOptional, Matches } from 'class-validator';

export enum Gender {
  male = 'male',
  female = 'female',
}

export class SalesValidatorDto {
  @IsNotEmpty({ message: 'Fecha Facturación: no debe estar vacío' })
  @Matches(/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/, {
    message: 'Fecha Facturación: Debe tener formato yyyy-mm-dd',
  })
  dateBilling: string;

  @IsEnum(Gender, {
    message: `Género: debe ser : ${Object.values(Gender).join(', ')}.`,
  })
  @IsNotEmpty({ message: 'Género: no debe estar vacío' })
  gender: Gender;

  @IsNotEmpty({ message: 'Fecha de nacimiento: no debe estar vacío' })
  @Matches(/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/, {
    message: 'Fecha Nacimiento: Debe tener formato yyyy-mm-dd',
  })
  birthdate: string;

  @IsNotEmpty({ message: 'Amount: no debe estar vacío' })
  amount: number;

  @IsOptional()
  age?: number;
}
